package main

import (
	"io"
	"os"
	"log"
	"fmt"
	"path/filepath"
	"sort"

)

var (
	err error
)



func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

func dirTree(out io.Writer, filePath string, printFiles bool) error   {
var fileFolder []string  //обьявили массив
///получаю домашнюю директорию пользователя
	visit := func(path string, info os.FileInfo, err error) error {
		fileFolder = append(fileFolder,path)
		return nil
	}
	err := filepath.Walk("./", visit)
	if err != nil {
		log.Fatal(err)
	}
	sort.Strings(fileFolder) //отсортировали массив
	var sortData []string
	//делаю итерацию по массиву удаляю из массива не нужные файлы
	for k := range fileFolder {
		if fileFolder[k] == "hw1.md" || fileFolder[k] == "dockerfile"  || fileFolder[k] == "./" {
			log.Println("Err Data is Missing ")
			}else {
			sortData = append(sortData,fileFolder[k])
		}
	}
	//делаем итерацию по отсортированному массиву
	for i := range sortData {
		getFileSize(sortData[i])
		fmt.Println(sortData[i]+getFileSize(sortData[i]))
	}
	return  err
}

//Размер файла в домашней директории
func getFileSize(path string) string  {
	var fileSize string
	fileInfo, _ := os.Stat(path)
	if !fileInfo.IsDir() {
		size := fileInfo.Size()
		if size == 0 {
			fileSize = " (empty)"
		} else {
			fileSize = fmt.Sprintf(" (%vb)", size)
		}
	}
	return fileSize
}